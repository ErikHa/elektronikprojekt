#!/usr/bin/python3

import sys
import lxml.etree
import re

def extract_row(tr, name_to_index):
    def text(e, report_error):
        # Transform HTML to text
        result = ''
        if e.tag == 'br':
            result += '\n'
            pass
        elif e.tag == 'sup':
            result += '^'
            result += e.text or ''
            pass
        else:
            result += e.text or ''
            pass
        if len(e):
            result += ''.join([ text(e1, report_error) for e1 in e ])
            pass
        result += e.tail or ''
        return result
    
    def get(key, report_error=True):
        try:
            index = name_to_index[key]
            return text(tr[index], report_error).rstrip()
        except KeyError as e:
            if report_error:
                print(e, name_to_index)
                raise
            return None
        pass

    name = get('Data Name')
    address = get('Address')
    size = get('Size')
    access = get('Access')
    default = (get('Initial', False) or get('Default')).replace(',', '')
    ranges = get('Range', False)
    if ranges == None:
        # Try with min ~ max
        rmin = get('Min', False)
        rmax = get('Max', False)
        if not rmin in [ None, '-'] and not rmax in [ None, '-' ]:
            ranges = "%s ~ %s" % (rmin, rmax)
            pass
        pass
    unit = get('Unit', False)
    return name, address, size, access, default, ranges, unit
    

class Row:

    def __init__(self, name, index, address, size, access, default,
                 ranges, unit):
        self.name = name
        self.index = int(index) if index else None
        self.address = address
        self.size = size
        self.access = access
        self.default = default
        self.ranges = ranges
        self.unit = unit
        pass

    def __getitem__(self, key):
        return getattr(self,key)
    
    def __repr__(self):
        return "Row(%s)" % ([self.name, self.index, self.address, self.size,
                             self.access, self.default, self.ranges, self.unit])

class ParsedRow(Row):

    def __init__(self, tr, name_to_index):
        (name, address, size, access, default,
         ranges, unit) = extract_row(tr, name_to_index)
        m = re.match('^([a-z_][a-z0-9_]*[a-z_])(?:_([0-9]+))?$',
                     name.lower()
                     .replace(' ', '_')
                     .replace('(shadow)', ''))
        if not (m and address.isdigit()):
            raise ValueError((name, address, size, access, default,
                              ranges, unit))
        def int_or_none(s):
            if s.isdigit() or s[0] == '-' and s[1:].isdigit():
                return(int(s))
            return None
        super(ParsedRow, self).__init__(
            name=m.group(1),
            index=m.group(2),
            address=int(address),
            size=int(size),
            access=set(access),
            default=int_or_none(default),
            ranges=ranges,
            unit=unit)

    def range(self, end):
        N = end.index - self.index
        adress_step = (end.address - self.address) / N
        default_step = (end.default - self.default) / N
        for i in range(1, N):
            yield Row(self.name, self.index + i,
                      int(self.address + i*adress_step),
                      self.size, self.access,
                      int(self.default + i*default_step),
                      self.ranges, self.unit)
        pass
        

        
class Table:

    def __init__(self, table):
        self.row = {}
        name_to_index = dict([ (e.text, i) for i,e in enumerate(table[0][0]) ])
        for i,tr in enumerate(table[1]):
            try:
                row = ParsedRow(tr, name_to_index)
                self.add(row)
                pass
            except ValueError:
                r1 = ParsedRow(table[1][i-1], name_to_index)
                r2 = ParsedRow(table[1][i+1], name_to_index)
                for r in r1.range(r2):
                    self.add(r)
                    pass
                pass
            pass
        pass

    def add(self, row):
        if row.index == None:
            # Ordinary row
            self.row[row.name] = row
            pass
        else:
            # Indirect row
            if not row.name in self.row:
                self.row[row.name] = {}
                pass
            self.row[row.name][row.index] = row
            pass
        pass

    def __iter__(self):
        def key(r):
            if isinstance(r[1], dict):
                # Sort indirect registers on first 
                return min([ r.address for r in r[1].values() ])
            return r[1].address
        for k,v in sorted(self.row.items(), key=key):
            yield v
            pass
        pass
    
    pass

def Parse(path):
    et = lxml.etree.HTML(open(path).read())
    title = None
    table = {}
    protocol = set()
    id = None
    def traverse(t):
        nonlocal title
        nonlocal table
        nonlocal protocol
        nonlocal id
        for st in t:
            if st.tag == 'title':
                # Save product name
                title = (st.text
                         .replace('/', '_')
                         .replace('(', '_')
                         .replace(')', '')
                         .replace('+', '')
                         .replace('.', '_'))
                if m := re.match('^2(.*)$', title):
                    title = f'DUAL-{m.group(1)}'
                    pass
                pass
            elif st.tag == 'table':
                # Only save control tables
                if id.startswith('control-table'):
                    table[id] = Table(st)
                    pass
                pass
            elif (st.tag == 'a' and id == 'control-table-data-address' and
                  (m := re.match('^.*/(protocol[^/]+).*$', st.get('href')))):
                protocol.add(m.group(1).replace('protocol', 'Protocol'))
            else:
                id = st.get('id') or id
                traverse(st)
                pass
            pass
        pass
    traverse(et)
    return(title, table, protocol)

for path in sys.argv[1:]:
    EEPROM = {}
    RAM = {}
    title, tables, protocol = Parse(path)
    print(title, protocol)
    if  re.match('^[A-X0-9]*-.*$', title):
        EEPROM[title] = tables['control-table-of-eeprom-area']
        RAM[title] = tables['control-table-of-ram-area']
        pass
    for model in sorted(EEPROM):
        with open("dynamixel/model/%s.py" % (model.lower().replace('-', '_')),
                  'w') as f:
            def expand_indirect_row(r):
                def ranges(r):
                    size = r[1].size
                    current = None
                    start = None
                    for k,v in sorted(r.items()):
                        if current == None:
                            start = v.address
                            pass
                        elif current + size != v.address:
                            yield range(start, current + size, size)
                            start = v.address
                            pass
                        current = v.address
                        pass
                    yield range(start, current + size, size)
                    pass
                return (f"'{r[1].name}': IndirectRow(name='{r[1].name}', "
                        f"addresses={list(ranges(r))}, "
                        f"size={r[1].size})")
            def expand_row(r):
                if isinstance(r, dict):
                    return expand_indirect_row(r)
                    pass
                else:
                    return (f"'{r.name}': "
                            f"{'W' in r.access and 'RW' or ''}Row("
                            f"name='{r.name}', "
                            f"address={r.address}, "
                            f"size={r.size}, default={r.default})")
                    pass
                
            def expand_table(t):
                for r in t:
                    yield expand_row(r)
                    pass
                pass
            py_model = model.replace('-', '_')
            indent = ',\n        '
            f.write(f'''
from dynamixel.servo import Servo
from dynamixel.servo import Row, RWRow, IndirectRow
from dynamixel.channel import {max(protocol)}

class {py_model}(Servo):
    EEPROM = {{
        {indent.join(expand_table(EEPROM[model]))}
    }}
    RAM = {{
        {indent.join(expand_table(RAM[model]))}
    }}
    PROTOCOL = {max(protocol)}

    def __init__(self, channel, id):
        super({py_model}, self).__init__(channel=channel, id=id)
''')
            pass
        pass
    pass
