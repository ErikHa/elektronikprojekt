#!/usr/bin/python3

from dynamixel.model.xm430_w210_t_r import XM430_W210_T_R
import dynamixel.channel
import time

if __name__ == '__main__':
    channel = dynamixel.channel.Channel(speed=1000000)
    servos = [ XM430_W210_T_R(channel, 1),
               XM430_W210_T_R(channel, 2),
               XM430_W210_T_R(channel, 3) ]
    for s in servos:
        s.torque_enable.write(0)
        print(s.model_number.read(), s.id.read())
        s.operating_mode.write(1)
        s.bus_watchdog.write(0) # Clear old watchdog error
        s.bus_watchdog.write(100) # 2 second timeout
        s.torque_enable.write(1)
        pass
    for i in range(331):
        print(i)
        for s in servos:
            s.goal_velocity.write(i)
            pass
        print([ s.present_position.read() for s in servos ])
        time.sleep(0.1)
    for i in range(330,100,-10):
        print(i)
        for s in servos:
            s.goal_velocity.write(i)
            pass
        print([ s.present_position.read() for s in servos ])
        time.sleep(0.1)
