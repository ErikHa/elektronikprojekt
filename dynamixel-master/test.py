#!/usr/bin/python3

from dynamixel.model.xm430_w210_t_r import XM430_W210_T_R
import dynamixel.channel
import time

if __name__ == '__main__':
    channel = dynamixel.channel.Channel(speed=1000000)
    servos = [ XM430_W210_T_R(channel, 1), 
               XM430_W210_T_R(channel, 2),
               XM430_W210_T_R(channel, 3) ]
    for s in servos:
        s.torque_enable.write(0)
        print(s.model_number.read(), s.id.read())
        s.operating_mode.write(1)
        s.velocity_limit.write(1023)
        print(s.velocity_limit.read())
        s.bus_watchdog.write(0) # Clear old watchdog error
        s.bus_watchdog.write(100) # 2 second timeout
        s.torque_enable.write(1)
        s.profile_acceleration.write(32767)
        pass
    for j in range(100):
        for i in range(5):
            #print(i+300)
            #print(s.velocity_limit.read())
            ind = 0
            #[s.goal_velocity.write(300) for s in servos]
            for s in servos:
                if ind == 0:
                    s.goal_velocity.write(500)
                    
                    ind = 1
                elif ind == 1:
                    s.goal_velocity.write(-500)
                    ind = 2
                pass
            print([ s.present_velocity.read() for s in servos ])
            time.sleep(0.1)
        for i in range(5):
            ind = 0
            for s in servos:
                    if ind == 0:
                        s.goal_velocity.write(-300)
                        
                        ind = 1
                    elif ind == 1:
                        s.goal_velocity.write(300)
                        ind = 0
                    pass
            #[s.goal_velocity.write(-300) for s in servos]
            print([ s.present_velocity.read() for s in servos ])
            time.sleep(0.1)
