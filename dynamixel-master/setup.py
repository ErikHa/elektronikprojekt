#!/usr/bin/env python
from distutils.core import setup, Extension

try:
    import os
    VERSION=os.environ['DYNAMIXEL_VERSION']
except KeyError:
    VERSION='_UNKNOWN_'
    pass

setup(
    name        = 'python-dynamixel',
    version     = VERSION,
    description = 'Python wrapper to the Dynamixel I/O library',
    long_description = 'Python wrapper to the Dynamixel I/O library',
    author      = 'Anders Blomdell',
    author_email = 'anders.blomdell@control.lth.se',
    url         = 'http://gitlab.control.lth.se/anders_blomdell/dynamixel.git',
    license     = 'Apache 2.0',
    platforms   = 'Linux',
    packages    = [ 'serial',
					'dynamixel',
					'dynamixel_sdk',
                    'dynamixel.channel',
                    'dynamixel.model',
                    'dynamixel.servo',]
)
